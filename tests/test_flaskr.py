import os
import tempfile

import pytest

from flaskr import create_app
from flaskr.db import init_db


@pytest.fixture()
def client():
    app = create_app()
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            init_db()
        yield client
    
    os.close(db_fd)
    os.unlink(app.config['DATABASE'])

def test_invalid_request(client):
    rv = client.get('/V1/stores')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lat=abc&lng=123')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lat=180&lng=0')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lat=0&lng=-95')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lat=123&lng=abc')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lat=0')
    assert 422 == rv.status_code

    rv = client.get('/V1/stores?lng=0')
    assert 422 == rv.status_code

def test_valid_request(client):
    rv = client.get('/V1/stores?lat=10.1&lng=-45.9')

    assert 200 == rv.status_code
    
def test_valid_response(client)  :
    rv = client.get('/V1/stores?lat=10.1&lng=-45.9')
    assert 'application/json' == rv.content_type

    json_response = rv.json
    assert type(json_response) == dict

    result_keys = json_response.keys()
    
    assert 'results' in result_keys
    assert  type(json_response['results']) == list

    for item in json_response['results']:
        assert type(item) == dict
        assert 'distance' in item.keys()
        assert type(item['distance']) == float
        assert item['distance'] >=0 and item['distance'] <= 6.5




