DROP TABLE IF EXISTS store;

CREATE TABLE "store" (
	"County"	TEXT,
	"LicenseNumber"	INTEGER,
	"OperationType"	TEXT,
	"EstablishmentType"	TEXT,
	"EntityName"	TEXT,
	"DBAName"	TEXT,
	"StreetNumber"	INTEGER,
	"StreetName"	TEXT,
	"AddressLine2"	TEXT,
	"AddressLine3"	TEXT,
	"City"	TEXT,
	"State"	TEXT,
	"ZipCode"	INTEGER,
	"SquareFootage"	INTEGER,
	"Latitude"	FLOAT,
  "Longitude"	FLOAT
)