import functools

from flask import (
    Blueprint, flash, g, current_app, redirect, render_template, request, make_response,session, url_for, jsonify
)

from flaskr.db import get_db
from flaskr.helpers import is_number

bp = Blueprint('stores', __name__)

@bp.route('/stores')
def stores():
    lat = request.args.get('lat',None)
    lng = request.args.get('lng',None)
    if all(map(lambda t: is_number(t) and abs(float(t)) <= 90., (lat,lng))):
        # magic
        return make_response(jsonify(
            {
                "results":[]
            }),200)

    return make_response(
        {
            "results":[],
            "error":"Invalid parameters"
        },422)