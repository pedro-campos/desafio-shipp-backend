import re
    
def is_number(n):
    return re.match(r"^-?[0-9][0-9,\.]+$", n or '') is not None
    